package com.demo.dto;

import lombok.Data;

@Data
public class UserInfoDto {
	private int userId;
	private String userName;
	private String address;
	private String email;
	private String password;
	private Integer age;
	private String phoneNo;
	private String gender;
	private String maritalStatus;
}
