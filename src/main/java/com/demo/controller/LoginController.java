package com.demo.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.demo.dto.UserInfoDto;

@Controller
public class LoginController extends WebMvcConfigurerAdapter {
	
	@RequestMapping(value="/login",method = RequestMethod.GET)
	public String init(UserInfoDto userDto) {
		//model.addAttribute("userInfoDto", new UserInfoDto());
		return "login";
	}
	
	@RequestMapping(value="/login",method = RequestMethod.POST)
	public String register(Model model,@Valid UserInfoDto user,BindingResult bindingResult) {
		
		if(bindingResult.hasErrors()) {
			//bindingResult.resolveMessageCodes("User name must fill");
			return "userSignUp";
		}
		
		model.addAttribute("userDto", user);
		return "userRegisterConfirm";
	}
}
