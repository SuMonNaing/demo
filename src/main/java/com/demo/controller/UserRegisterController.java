package com.demo.controller;

import java.util.LinkedHashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.demo.dto.UserInfoDto;

@Controller
public class UserRegisterController extends WebMvcConfigurerAdapter {
	
	@ModelAttribute("genderMap")
	Map<Integer, String> getGender() {
	Map<Integer, String> genderMap= new LinkedHashMap<Integer, String>();
	genderMap.put(1, "Male");
	genderMap.put(2, "Female");
	return genderMap;
	}
	
	
	@RequestMapping(value="/register",method = RequestMethod.GET)
	public String init(UserInfoDto userDto) {
		//model.addAttribute("userInfoDto", new UserInfoDto());
		return "userSignUp";
	}
	
	@RequestMapping(value="/register",method = RequestMethod.POST)
	public String register(Model model,@Valid UserInfoDto user,BindingResult bindingResult) {
		
		if(bindingResult.hasErrors()) {
			//bindingResult.resolveMessageCodes("User name must fill");
			return "userSignUp";
		}
		
		model.addAttribute("userDto", user);
		return "userRegisterConfirm";
	}
}
